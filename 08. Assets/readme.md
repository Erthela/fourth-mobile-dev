Для добавления изображений требуется добавить некоторые описания их в файл проект `pubspec.yaml`.

### Для добавления шрифтов:
```yaml
flutter:
  fonts:
    - family: NotoSans
      fonts:
        - asset: fonts/NotoSans/NotoSans-Regular.ttf
        - asset: fonts/NotoSans/NotoSans-Italic.ttf
          style: italic
        - asset: fonts/NotoSans/NotoSans-Bold.ttf
          weight: 700
        - asset: fonts/NotoSans/NotoSans-BoldItalic.ttf
          weight: 700
          style: italic
```
Соблюдение отступов для `*.yaml` файлов обязательно.
- `style` описывает стиль текста (в данном случае курсивное написание: italic)
- `weight` описывает жирность текста (100-900, только круглые значения)

### Для добавления изображений:
```yaml
flutter:
  assets:
    - assets/
```
Вы создаёте папку assets в корне проекта и туда добавляете фотографии.

Если мы не хотим добавлять все фотографии из дериктории, а хотим добавить какие-то конкретные, то тогда нужно описать их явно:
```yaml
flutter:
  assets:
    - assets/image1.png
    - assets/image2.png
    - assets/image3.png
```

Объединённо с шрифтами блок будет выглядеть как:
```yaml
flutter:
  fonts:
    - family: NotoSans
      fonts:
        - asset: fonts/NotoSans/NotoSans-Regular.ttf
        - asset: fonts/NotoSans/NotoSans-Italic.ttf
          style: italic
        - asset: fonts/NotoSans/NotoSans-Bold.ttf
          weight: 700
        - asset: fonts/NotoSans/NotoSans-BoldItalic.ttf
          weight: 700
          style: italic
  assets:
    - assets/
```