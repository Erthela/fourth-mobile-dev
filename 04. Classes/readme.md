4. Классы
   https://dart.dev/guides/language/language-tour#classes
### Классы
#### Использование конструкторов
- Мы можем создать у класса конструктор, который будет называться так же, как название
  самого класса. Кроме того мы можем создать конструктор, который будет
  называться `<Название класса>.<идентификатор>`.
  Также в dart необязательно указывать ключевое слово new для создания экземпляра класса
  ```dart
   var p1 = Point(2, 2);
   var p2 = Point.fromJson({'x': 1, 'y': 2});
  ```
  У некоторых классов есть константные конструкторы:
  ```dart
  var p = const ImmutablePoint(2, 2);
  ```
  Если мы хотим создать константный конструктор из констант, то мы можем опустить часть ключевых слов const:
  ```dart
  // плохой пример
  const pointAndLine = const {
    'point': const [const ImmutablePoint(0, 0)],
    'line': const [const ImmutablePoint(1, 10), const ImmutablePoint(-2, 11)],
  };
  // хороший пример
  const pointAndLine = {
   'point': [ImmutablePoint(0, 0)],
   'line': [ImmutablePoint(1, 10), ImmutablePoint(-2, 11)],
  };
  ```
#### Поля класса (переменные экземпляров/инстансов)
- Все поля имеют неявные getter'ы. Не `final` и не `late final` без инициализации имеют setter методы.
```dart
class Point {
  double? x; // Declare instance variable x, initially null.
  double? y; // Declare y, initially null.
}

void main() {
  var point = Point();
  point.x = 4; // Use the setter method for x.
  assert(point.x == 4); // Use the getter method for x.
  assert(point.y == null); // Values default to null.
}
```
- Если поле финальное и не `late`, то его нужно инициализировать в конструкторе, в списке инициализации конструктора или
  при создании.
  ```dart
  class ProfileMark {
    final String name;
    final DateTime start = DateTime.now();
  
    ProfileMark(this.name);
    ProfileMark.unnamed() : name = '';
  }
  ```
Если требуется вызвать конструктор из родительского класса, то его нужно имплементировать в ребёнке
(сделать ссылку на родительский конструктор)
#### Factory конструкторы
Не создают инстанс. Один из вариантов использования: инициализация `final` переменных.
```dart
class Logger {
  final String name;
  bool mute = false;

  // _cache is library-private, thanks to
  // the _ in front of its name.
  static final Map<String, Logger> _cache =
      <String, Logger>{};

  factory Logger(String name) {
    return _cache.putIfAbsent(
        name, () => Logger._internal(name));
  }

  factory Logger.fromJson(Map<String, Object> json) {
    return Logger(json['name'].toString());
  }

  Logger._internal(this.name);

  void log(String msg) {
    if (!mute) print(msg);
  }
}
```
`Note: У фабричного конструктора нет доступа к this`
```dart
var logger = Logger('UI');
logger.log('Button clicked');

var logMap = {'name': 'UI'};
var loggerJson = Logger.fromJson(logMap);
```

### Методы
- Функции, описывающие поведение класса
#### Методы экземпляров
 ```dart
 import 'dart:math';
 class Point {
  double x = 0;
  double y = 0;
  Point(this.x, this.y);

  double distanceTo(Point other) { //пример метода экземпляра
    var dx = x - other.x;
    var dy = y - other.y;
    return sqrt(dx * dx + dy * dy);
  }
 }
 ```
#### Операторы
Переопределение операторов
```dart
<	+	|	>>>
>	/	^	[]
<=	~/	&	[]=
>=	*	<<	~
–	%	>>	==
```
##### Пример
```dart
// dart language tour https://dart.dev/guides/language/language-tour#methods
class Vector {
 final int x, y;

 Vector(this.x, this.y);

 Vector operator +(Vector v) => Vector(x + v.x, y + v.y);
 Vector operator -(Vector v) => Vector(x - v.x, y - v.y);

 // Operator == and hashCode not shown.
 // ···
}

void main() {
 final v = Vector(2, 3);
 final w = Vector(2, 2);

 assert(v + w == Vector(4, 5));
 assert(v - w == Vector(0, 1));
}
```
#### Геттеры / сеттеры
Специальные методы, которые предоставляют доступ к полям класса.
```dart
class Rectangle {
  double left, top, width, height;
  
  Rectangle(this.left, this.top, this.width, this.height);
  
  // Define two calculated properties: right and bottom.
  double get right => left + width;
  set right(double value) => left = value - width;
  double get bottom => top + height;
  set bottom(double value) => top = value - height;
}

void main() {
  var rect = Rectangle(3, 4, 20, 15);
  assert(rect.left == 3);
  rect.right = 12;
  assert(rect.left == -8);
}
```
### Абстрактные классы
Требуются для описания шаблона поведения некоторого набора классов
(для дочерних классов).
#### Пример
```dart
// This class is declared abstract and thus
// can't be instantiated.
abstract class AbstractContainer {
  // Define constructors, fields, methods...

  void updateChildren(); // Abstract method.
}
```
### Интерфейсы
Каждый класс неявно определяет интерфейс, содержащий его элементы.
Для реализации интерфейса требуется ключевое слово `implements` и можно указать 
несколько интерфейсов после этого.
#### Пример
```dart
// A person. The implicit interface contains greet().
class Person {
  // In the interface, but visible only in this library.
  final String _name;

  // Not in the interface, since this is a constructor.
  Person(this._name);

  // In the interface.
  String greet(String who) => 'Hello, $who. I am $_name.';
}

// An implementation of the Person interface.
class Impostor implements Person {
  String get _name => '';

  String greet(String who) => 'Hi $who. Do you know who I am?';
}

String greetBob(Person person) => person.greet('Bob');

void main(List<String> arguments) {
  print(greetBob(Person('Kathy')));
  print(greetBob(Impostor()));
}
```
### Зависимости (наследование)
Для наследования класса требуется у подкласса указать какой класс он расширяет через `extends`.
Для обращения к классу-родителю требуется использовать ключевое слово `super`.
```dart
class Television {
  void turnOn() {
    _illuminateDisplay();
    _activateIrSensor();
  }
  // ···
}

class SmartTelevision extends Television {
  void turnOn() {
    super.turnOn();
    _bootNetworkInterface();
    _initializeMemory();
    _upgradeApps();
  }
  // ···
}
```
### Перегрузка методов (переопределение)
Подкласс может перегрузить методы, геттеры, сеттеры и операторы родительского класса.
Для отметок перегрузки требуется воспользоваться аннотацией `@override`

```dart
class Television {
  // ···
  set contrast(int value) {...}
}

class SmartTelevision extends Television {
  @override
  set contrast(num value) {...}
  // ···
}
```

Для перегруженных методов есть ряд ограничений:
- Тип возвращаемого значения исходного и перегруженного методов должны совпадать
- Типы аргументов у исходного и перегруженного метода должны совпадать или быть подтипами исходных
- Количество аргументов исходного и перегруженного методов должны совпадать
- Метод с дженериками должен быть переопределён только методом с другими дженериками

### Расшираение методов
Возможность дополнения функциональности существующих классов и методов
```dart
print('42'.parseInt()); // Use an extension method.

extension NumberParsing on String {
  int parseInt() {
    return int.parse(this);
  }
  // ···
}
```

## Добаление функция для класса
### Mixins
Способо переиспользоания кода класса в множестве классовых иерархий.
#### Использование mixin
```dart
class Musician extends Performer with Musical {
  // ···
}

class Maestro extends Person
    with Musical, Aggressive, Demented {
  Maestro(String maestroName) {
    name = maestroName;
    canConduct = true;
  }
}
```
#### Реализация mixin
1. Создать класс, наследуемый от Object без конструктора
2. Использовать ключевое слово `mixin`
```dart
mixin Musical {
  bool canPlayPiano = false;
  bool canCompose = false;
  bool canConduct = false;

  void entertainMe() {
    if (canPlayPiano) {
      print('Playing piano');
    } else if (canConduct) {
      print('Waving hands');
    } else {
      print('Humming to self');
    }
  }
}
```
Также можно ограничить возможные классы, которые смогу использовать mixin:
используя ключевое слово `on`.
Он должен расширять или реализовывать этот класс.
```dart
class Musician {
  // ...
}
mixin MusicalPerformer on Musician {
  // ...
}
class SingerDancer extends Musician with MusicalPerformer {
  // ...
}
```

## Статика (static)
Используется если требуеся создать переменную не для инстанса (конкретной сущности), а для класса целиком,
то нужно использовать ключевое слово static.
Также используется для описания поведения (метода) целого класса.
```dart
import 'dart:math';

class Point {
  double x, y;
  Point(this.x, this.y);

  static double distanceBetween(Point a, Point b) {
    var dx = a.x - b.x;
    var dy = a.y - b.y;
    return sqrt(dx * dx + dy * dy);
  }
}

void main() {
  var a = Point(2, 2);
  var b = Point(4, 4);
  var distance = Point.distanceBetween(a, b);
  assert(2.8 < distance && distance < 2.9);
  print(distance);
}
```
P.S. Рассмотрите возможность использования функций верхнего урвоня вместо статических методов.