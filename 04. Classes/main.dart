void main() {
  var st = Student();
  print(st.name);
  print(st.grade);
  print(st.age);
}

class Student {
  String name;
  int age;
  final int grade;

  factory Student() {
    var age = 8;
    var grade = age - 7;
    return Student._internal('Dima', age, grade: grade);
  }

  Student._internal(this.name, this.age, {this.grade = 1});


}