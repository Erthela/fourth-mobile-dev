# 4 Курс: Мобильная Разработка

Задание на семестр:
1. Реализовать приложение по [макету](https://www.figma.com/file/HV6PCsUkQczpQYzm9rayER/Weather-app?node-id=0%3A1)
2. Срок: до 13 декабря

Полезные библиотеки для реализации приложения:
- [Свайпер для карточек](https://pub.dev/packages/card_swiper)
- [Неоморфизм кнопки](https://pub.dev/packages/flutter_neumorphic)
- [Расширяемое нижнее модальное окно](https://pub.dev/packages/expandable_bottom_sheet)

Если ваш диплом связан с мобильной разработкой (flutter, java, kotlin и т.д.), то
вы можете начать реализовывать ваш диплом, но должны быть покрыты более 75% тем:
01. Basic Widgets (Базовые компоненты)
02. Style (Стилизация компонентов)
03. Assets (Подключение ассетов: фотографий, шрифтов, иконок и т.д.)
04. View_ViewGroup (Компоненты связанные с группировкой других компонентов)
05. Navigation (Навигация по приложению, изменение анимаций переходов между экранами)
06. Storage (Взаимодействие с объектами из внутреннего хранилища устройства)
07. External Data (web-api requests, запросы к внешним серверам, сбор и парсинг данных)
08. External libs (Внешние библиотеки)
09. stateManagement (Управление состояние виджетов, как и где передавать. Разные подходы:
    передача ребёнку, Provider, Redux и т.д.)

Полезные ссылки:
- [Первая коллекция Codewars](https://www.codewars.com/collections/intro-dart)
- [Вторая коллекция Codewars](https://www.codewars.com/collections/arrays-dart)
- [Коллекция с обязательными заданиями](https://www.codewars.com/collections/final-dart)
- [Классный плейлист (Изучаем Flutter с Кокориным, рус)](https://www.youtube.com/playlist?list=PLtXyatp5fW30ksCelZf6Ny6YpqfzXMk9u)
- [Пиксельная мера (docs, eng)](https://api.flutter.dev/flutter/dart-ui/FlutterView/devicePixelRatio.html)
- [Ещё один классный плейлист (flutter.dev, eng)](https://www.youtube.com/watch?v=b_sQ9bMltGU&list=PLjxrf2q8roU23XGwz3Km7sQZFTdB996iG)
- [Подключение по WIFI (abd, eng)](https://proandroiddev.com/utilizing-adb-for-daily-tasks-b52a27715ee5)