import 'dart:async';

void main() {
  // runSimpleFutureExample();
  // runFutureWithAwaitExample();
  // runStreamSimpleExample();
  // runStreamAwaitedSimpleExample();
  // runBroadcastStreamExample();
  // runBroadcastStreamExampleSub();
  yieldFuncStreamExample();
}

runSimpleFutureExample({
  String question = 'В чем смысл жизни и всего такого?',
}) {
  print('Start of future example');
  Future(() {
    print('Вопрос: $question');
    if (question.length > 10) {
      return 42;
    } else {
      throw Exception('Вы задали недостаточно сложный вопрос.');
    }
  }).then((result) {
    print('Ответ: $result');
  }).catchError((error) {
    print('Ошибка. $error');
  });
  print('Finish of future example');
}

runFutureWithAwaitExample({
  String question = 'В чем смысл жизни и всего такого?',
}) async {
  print('Start of future example');
  try {
    final result = await _getAnswerForQuestion(
      'В чем смысл жизни и всего такого?',
    );
    print('Ответ: $result');
  } catch (error) {
    print('Ошибка. $error');
  }
  print('Finish of future example');
}

Future<int> _getAnswerForQuestion(String question) => Future(() {
      print('Вопрос: $question');
      if (question.length > 10) {
        return 42;
      } else {
        throw Exception('Вы задали недостаточно сложный вопрос.');
      }
    });

/*
* Здесь используем fromIterable. Создаём в массиве пять чисел, подписываемся на стрим через метод listen и выводим на консоль:
* */

void runStreamSimpleExample() {
  print('Simple stream example started');
  final stream = Stream.fromIterable([1, 2, 3, 4, 5]);
  stream.listen((number) {
    print('listener: $number');
  });
  print('Simple stream example finished');
}

void runStreamAwaitedSimpleExample() async {
  print('Simple stream example with await started');
  final stream = Stream.fromIterable([1, 2, 3, 4, 5]);
  await for (final number in stream) {
    print('Number: $number');
  }
  print('Simple stream example with await finished');
}

///пример broadcast стрима
void runBroadcastStreamExample() {
  print('Broadcast stream example started');
  final streamController = StreamController.broadcast();
  streamController.stream.listen((number) {
    print('Listener 1: $number');
  });
  streamController.stream.listen((number) {
    print('Listener 2: $number');
  });
  streamController.sink.add(1);
  streamController.sink.add(2);
  streamController.sink.add(3);
  streamController.sink.add(4);
  streamController.sink.add(5);
  streamController.close();
  print('Broadcast stream example finished');
}

///пример broadcast стрима с подпиской
void runBroadcastStreamExampleSub() {
  print('Broadcast stream example started');
  final streamController = StreamController.broadcast();
  streamController.stream.listen((number) {
    print('Listener 1: $number');
  });
  StreamSubscription? sub2;
  sub2 = streamController.stream.listen((number) {
    print('Listener 2: $number');
    if (number == 3) {
      sub2?.cancel();
    }
  });
  streamController.sink.add(1);
  streamController.sink.add(2);
  streamController.sink.add(3);
  streamController.sink.add(4);
  streamController.sink.add(5);
  streamController.close();
  print('Broadcast stream example finished');
}

Future<int> sumStream(Stream<int> stream) async {
  var sum = 0;
  await for (var value in stream) {
    print(value);
    sum += value;
  }
  return sum;
}

Stream<int> countStream(int to) async* { //for Stream return
  for (int i = 1; i <= to; i++) {
    yield i;
  }
}

yieldFuncStreamExample() async {
  var stream = countStream(10);
  var sum = await sumStream(stream);
  print(sum); // 55
}