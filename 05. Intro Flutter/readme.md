5. Введение в Flutter
- [Классный плейлист](https://www.youtube.com/playlist?list=PLtXyatp5fW30ksCelZf6Ny6YpqfzXMk9u)
- Разбор примера, который получается после генерации нового проекта
  - прям построчный разбор примера
- Файловая структура
  - что такое pubspec и зачем он нужен
  - assets (как библиотека)
- Глобальные стилевые библиотеки Material vs Cupertino
  - отличия
  - назначения
- Показать создание новых элементов (классы)
- Создание проекта с нуля (удалить всё и начать писать своё)
- Графическое ядро `Skia`
