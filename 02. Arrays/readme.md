2. Массивы
- Констркуторы (`List<E>.`)
  - `empty({bool growable = false})` - создаёт пустой лист
  - `filled(int length, E fill, {bool growable = false})` - массив заполненный элементами `fill`
  - `from(Iterable elements, {bool growable = true})` - массив из другого массива (желательно для оптимизации сделать его нерасширяемым)
  - `generate(int length, E generator(int index), {bool growable = true})` - функция генерации массива (можно заменить literal array constructor)
  - `of(Iterable<E> elements, {bool growable = true})`- массив из другого массива (желательно для оптимизации сделать его нерасширяемым)
  - `unmodifiable` - создаётся константный лист = `const []`
```dart
List<String> foo = new List.from(<int>[1, 2, 3]); // okay until runtime.
List<String> bar = new List.of(<int>[1, 2, 3]); // analysis error
```
---
```dart
var foo = new List.from(<int>[1, 2, 3]); // List<dynamic>
var bar = new List.of(<int>[1, 2, 3]); // List<int>
```
---
Но лучше не использовать не то ни другой подходы, если можно использовать `array-literal`
- Свойства (поля)
  - `first ↔ E` (RW) - возвращает первый элемент
  - `isEmpty → bool` (R) - возвращает `true`, если лист пустой
  - `isNotEmpty → bool` (R) - возвращает `true`, если лист непустой
  - `iterator → Iterator<E>` (R) - возвращает переменную типа `Iterator`
  - `last ↔ E` (R) - возвращает последний элемент
  - `length → int` (RW) - длина
  - `reversed → Iterable<E>` (R) - возращает объект с обратным порядком элементов
  - `single → E` (R) - проверяет состоли ли лист из 1 элемента, иначе **ошибка**
- Методы
  ```dart
  // добавление элемента в конец (расширяет размер на 1)
  add(E value) → void 
  // добавляет элементы из другого iterable
  addAll(Iterable<E> iterable) → void
  // проверяет удовлетворяет ли любой элемент массива условию bool test(E element)
  any(bool test(E element)) → bool
  // представление массива, как map, где index - key
  asMap() → Map<int, E>
  // опустошает массив
  clear() → void
  // проверяет содержится ли элемент в массиве
  contains(Object? element) → bool
  // достать элемент по индексу
  elementAt(int index) → E
  // проверяет удовлетворяет ли каждый элемент массива условию bool test(E element)
  every(bool test(E element)) → bool
  // расширяет каждый элемент до 0 или множества элементов
  expand<T>(Iterable<T> f(E element)) → Iterable<T>
  
  var pairs = [[1, 2], [3, 4]];
  var flattened = pairs.expand((pair) => pair).toList();
  print(flattened); // => [1, 2, 3, 4];
  
  var input = [1, 2, 3];
  var duplicated = input.expand((i) => [i, i]).toList();
  print(duplicated); // => [1, 1, 2, 2, 3, 3]
  
  // заполнить некоторую область значениями fillValue
  fillRange(int start, int end, [E? fillValue]) → void
  
  var list = List.filled(5, -1);
  print(list); //  [-1, -1, -1, -1, -1]
  list.fillRange(1, 3, 4);
  print(list); //  [-1, 4, 4, -1, -1]
  
  // вовзращает первый элемент, который соответствует test
  // если orElse пуст, то если элемент не надйен, то падает StateError
  firstWhere(bool test(E element), {E orElse()?}) → E
  // преобразует коллекцию до одного элемента
  fold<T>(T initialValue, T combine(T previousValue, E element)) → T
  
  // пример суммы всего массива
  iterable.fold(0, (prev, element) => prev + element);
  
  // привязать other элемент перед this (lazy)
  followedBy(Iterable<E> other) → Iterable<E>
  // перебор массива
  forEach(void f(E element)) → void
  // создаёт подмассив типа iterable
  getRange(int start, int end) → Iterable<E>
  
  List<String> colors = ['red', 'green', 'blue', 'orange', 'pink'];
  Iterable<String> range = colors.getRange(1, 4);
  range.join(', ');  // 'green, blue, orange'
  colors.length = 3;
  range.join(', ');  // 'green, blue'
  
  // индекс первого элемента 
  indexOf(E element, [int start = 0]) → int
  // индекс первого элемента, который соответствует test
  indexWhere(bool test(E element), [int start = 0]) → int
  // вставить элемент в определённое место (не заменить)
  insert(int index, E element) → void
  // вставить iterable по индексу
  insertAll(int index, Iterable<E> iterable) → void
  // преобразует все элементы в строки и склеивает с разделителем
  join([String separator = ""]) → String
  // индекс последнего элемента 
  lastIndexOf(E element, [int? start]) → int
  
  var notes = ['do', 're', 'mi', 're'];
  notes.lastIndexOf('re', 2); // 1
  notes.lastIndexOf('re');  // 3
  notes.lastIndexOf('fa');  // -1
  
  // индекс последнего элемента, который соответствует test
  lastIndexWhere(bool test(E element), [int? start]) → int
  
  var notes = ['do', 're', 'mi', 're'];
  notes.lastIndexWhere((note) => note.startsWith('r'));       // 3
  notes.lastIndexWhere((note) => note.startsWith('r'), 2);    // 1
  notes.lastIndexWhere((note) => note.startsWith('k'));    // -1
  
  // последний элемент, который соответствует test
  lastWhere(bool test(E element), {E orElse()?}) → E
  // возвращает новый iterable (lazy), созданный из T f(E e) 
  map<T>(T f(E e)) → Iterable<T>
  // упрощает массив до одного элемента
  reduce(E combine(E value, E element)) → E
  
  // sum of elements of iterable
  iterable.reduce((value, element) => value + element);
  
  // исключить элемент по зачению
  remove(Object? value) → bool
  // исплючить элемент по индексу
  removeAt(int index) → E
  // достать последний элемент
  removeLast() → E
  // удалить часть списка
  removeRange(int start, int end) → void
  // исключить элементы, которые соответствуют test
  removeWhere(bool test(E element)) → void
  
  var numbers = ['one', 'two', 'three', 'four'];
  numbers.removeWhere((item) => item.length == 3);
  numbers.join(', '); // 'three, four'
  
  // заместить подлист на новый набор элементов
  replaceRange(int start, int end, Iterable<E> replacements) → void
  
  var list = [1, 2, 3, 4, 5];
  list.replaceRange(1, 4, [6, 7]);
  list.join(', '); // '1, 6, 7, 5'
  
  // оставляет только элементы, которые соответствуют test
  retainWhere(bool test(E element)) → void
  
  var numbers = ['one', 'two', 'three', 'four'];
  numbers.retainWhere((item) => item.length == 3);
  numbers.join(', '); // 'one, two'
  
  // заменить с index this на iterable
  setAll(int index, Iterable<E> iterable) → void
  
  var list = ['a', 'b', 'c', 'd'];
  list.setAll(1, ['bee', 'sea']);
  list.join(', '); // 'a, bee, sea, d'
  
  // заменить с start до end в this на элементы из iterable с пропуском skipCount элементов
  setRange(int start, int end, Iterable<E> iterable, [int skipCount = 0]) → void
  
  var list1 = [1, 2, 3, 4];
  var list2 = [5, 6, 7, 8, 9];
  // Copies the 4th and 5th items in list2 as the 2nd and 3rd items
  // of list1.
  list1.setRange(1, 3, list2, 3);
  list1.join(', '); // '1, 8, 9, 4'
  
  // перемешать элементы листа
  shuffle([Random? random]) → void
  // ищет единственное соответствие с test, если больше одного, то StateError, иначе orElse
  // если он не объявлен, то StateError 
  singleWhere(bool test(E element), {E orElse()?}) → E
  // возвращает новый массив без count элементов this
  skip(int count) → Iterable<E>
  // пропускает все элементы до удовлетворяющего test
  skipWhile(bool test(E value)) → Iterable<E>
  // сортировка
  sort([int compare(E a, E b)?]) → void
  // создаёт подлист
  sublist(int start, [int? end]) → List<E>
  // достать первые count элементов
  take(int count) → Iterable<E>
  // брать пока не найдётся элемент удовлетворяющий test
  takeWhile(bool test(E value)) → Iterable<E>
  // cast Iterable -> List
  toList({bool growable = true}) → List<E>
  // cast Iterable -> Set
  toSet() → Set<E>
  // cast Iterable -> Strinf
  toString() → String
  // достать все элементы, подходящие под test
  where(bool test(E element)) → Iterable<E>
  // достать все элементы, подходящие под тип T
  whereType<T>() → Iterable<T>
  ```
- Операторы 
  - ```dart
    + // склеить 
    ```
  - ```dart
    == // сравнение
    ```
  - ```dart
    [] // взять по индексу
    ```
  - ```dart
    []= // установить заменить по индексу
    ```
- Статические методы
  ```dart
  //преобразовать List<S> -> List<T>
  castFrom<S, T>(List<S> source) → List<T>

  // скопировать элементы одного листа в другой
  copyRange<T>(List<T> target, int at, List<T> source, [int? start, int? end]) → void

  // написать из iterable в list
  writeIterable<T>(List<T> target, int at, Iterable<T> source) → void
  ```
